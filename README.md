# proj3-JSA
Vocabulary anagrams game for primary school English language learners (ELL)

## Author: Qi Han
## Email: qhan@uoregon.edu 


# Tasks

* Review the minijax project to be familiar with by running them separately. You need to understand them to do this project.

* Replace the form interaction in flask_vocab.py with AJAX interaction. 

* Create Dockerfile and put inside vocab folder.

* Submit credentials.ini file in canvas.

